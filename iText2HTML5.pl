#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

package iText2HTML5;

use Any::Moose;
use Any::Moose (
    'X::Types::Path::Class' => [qw/ File /]
    );

use File::Basename qw/ fileparse /;
use Encode qw/ find_encoding /;
use URI::Escape qw/ uri_unescape /;
use HTML::Entities qw/ encode_entities /;

with any_moose('X::Getopt');

has source => (
    is       => 'rw',
    isa      => File,
    required => 1,
    coerce   => 1,
);

has output => (
    is      => 'rw',
    isa     => File,
    default => './output.html',
    coerce  => 1,
);

has encoding => (
    is      => 'rw',
    isa     => 'Str',
    default => 'utf-8',
);

has title => (
    is         => 'rw',
    isa        => 'Str',
    lazy_build => 1,
);

no Any::Moose;

sub _build_title {
    return ( fileparse( $_[0]->source->basename ) )[0];
}

sub run {
    my $self = shift;
    my $enc  = find_encoding( $self->encoding );

    my $text = $self->source->slurp();
    my $html = $enc->decode( $text );
    my ( $body ) = ( $html =~ m{<body>(.+?)</body>}s );

    # remove <br>
    $body =~ s{<p class="p2"><br></p>}{}sig;

    # support ruby tags
    $body =~ s{<a href="rubytext:((?:[%][0-9A-F]{2})+)">(.+?)</a>}{
        my $text   = encode_entities( $enc->decode( uri_unescape($1) ) );
        my $target = $2;

        "<ruby>${target}<rt>$text</rt></ruby>";
    }sige;

    my $out = $enc->encode(<<"__HTML5__");
<!DOCTYPE html>
<html>
<head>
  <meta charset="@{[ $self->encoding ]}">
  <title>@{[ $self->title ]}</title>
</head>
<body>
@{[ $body ]}
</body>
</html>
__HTML5__

    my $fh = $self->output->openw or die "Failed to open file: @{[ $self->output->stringify ]}: ${!}";
    $fh->print($out);
    $fh->close;
    
    exit;
}

no Any::Moose;

__PACKAGE__->meta->make_immutable;

package main;

iText2HTML5->new_with_options->run;

=head1 NAME

iText2HTML5.pl - Convert iText Pro HTML to HTML5

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify it under the same terms as Perl itself.

=cut

