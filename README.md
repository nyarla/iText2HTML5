# これは何ですか？

このスクリプトは、

* [iText Pro](http://homepage.mac.com/lightway/iTextPro.html)

という、シェアウェアのテキストエディタが吐き出すHTMLファイルを、
HTML5に添うようにそれっぽく変換するスクリプトです。

# 作った目的

iText Proはデフォのファイル形式がリッチテキストなんですが、
これをePubとかに加工するのはムリポな感じだったので、
iText ProにHTML吐かせれば良いんじゃーとか思ったんですが、
その吐くHTMLがなんだからレガシィー！　な感じだったので、
やりやすいようにHTML5っぽく加工しよう！
というのが事の始まり。正味十分ぐらいで書いた。

# 制限とか注意点

iText Proが吐くHTMLの構造をすべて把握しきれてないので、
サポートしてない感じの構文が多数あると思います。

まあ一応ルビとか傍点は対応してるかと思います。

# 使い方

    $ ./iText2HTML5.pl --source=./iTextPro.html

他のオプションとかは`./iText2HTML5.pl --help`とかソースをみてけれ。

# 作者と連絡先

*にゃるら*こと*岡村直樹*

* *nyarla[ at ]thotep.net*
* [Twitter: @nyarla](https://twitter.com/nyarla)
